/**
 * Created by IntelliJ Idea
 * User: jzbrooks
 * Date: 2/12/14
 * Time: 9:59 PM
 *
 * Linked-List class
 */
public class List {
    Node front, back;
    int size;

    List() {
        front = back = null;
        size = 0;
    }

    public void append(String key, int pos) {
        Node p = new Node(key, pos);
        if(isEmpty()) front=p;
        else back.next=p;
        back = p;
        size++;
    }

    public void removeFirst() {
        if (front == null)
            return;
        else {
            if (front == back) {
                front = null;
                back = null;
                size--;
            } else {
                front = front.next;
                size--;
            }
        }
    }

    public Node pop() {
        Node top = first();
        removeFirst();
        return top;
    }

    public Node first() {return front;}
    public Node last() {return back;}
    public boolean isEmpty() {return size==0;}
    public boolean has_one_item() {return front==back;}
}