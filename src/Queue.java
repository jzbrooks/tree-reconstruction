/**
 * Created by IntelliJ Idea
 * User: jzbrooks
 * Date: 2/15/14
 * Time: 2:40 PM
 */
public class Queue {
    Node front, back;
    int n;

    public Queue() {
        front = null; back = null; n = 0;
    }

    public void enqueue (Node entry) {
        Node p = entry;
        if (isEmpty()) front = p; else back.next = p;
        back = p;
        n++;
    }

    public Node dequeue() {
        Node x = front;
        front = front.next;
        if (front == null) back = null;
        n--;
        return x;
    }

    public String front() {
        return front.key;
    }

    public boolean find(String key) {
        for(Node c=front; c!=null; c=c.next) {
            if (c.key==key) return true;
        }
        return false;
    }

    boolean isEmpty() { return n == 0; }
    int size() { return n; }
}