/**
 * Created by IntelliJ Idea
 * User: jzbrooks
 * Date: 2/12/14
 * Time: 9:59 PM
 */
public class Node {
    String key;

    // For template
    int index;
    Node next;
    Node(String charact, int idx) {
        index=idx; key=charact;}

    // For Level Order Queue
    Node(String k) {key=k; next=null;}

    // For Tree
    Node parent,leftMostChild,rightSibling;
    boolean isLeftMost;
    Node(String s, Node par, Node leftMostChild, Node rightSibling) {
        key=s;
        parent=par;
    }

    // Returns true if node has a right sibling
    public boolean hasRightSibling() {return this.rightSibling!=null;}
}