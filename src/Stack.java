/**
 * Created by IntelliJ Idea
 * User: jzbrooks
 * Date: 2/13/14
 * Time: 10:03 PM
 */
public class Stack {
    Node top, temp;
    int n;

    Stack() { top = null; n = 0; }

    void push (String key, int idx) {
        if (top!=null)
            temp = top;
        top = new Node (key, idx);
        top.next = temp;
        n++;
    }

    void deleteTop( ) {
        top = top.next;
        n--;
    }

    String topKey() {
        return top.key;
    }

    boolean isEmpty() {return top==null;}
    int size() {return n;}
}