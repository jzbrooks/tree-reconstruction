import java.io.*;

/**
 * Created by IntelliJ Idea
 * User: jzbrooks
 * Date: 2/12/14
 * Time: 9:48 PM
 */
public class Main {

    public static void main( String[] args ) {
        BufferedReader in = new BufferedReader(
                new InputStreamReader(System.in));

        String pre="", post="";
        List preorder_list;
        NBTree tree;

        // Main Loop
        try {
            while( (pre=in.readLine()) != null )
            {
                post=in.readLine();

                // Parse Input Strings for tree building alg
                preorder_list = NBTree.buildTemplate(pre,post);
                tree = new NBTree();
                tree.buildTree(preorder_list);

                //-- OUTPUT
                System.out.print("Pre:   ");
                tree.preorder();
                System.out.println();
                System.out.print("Post:  ");
                tree.postorder();
                System.out.println();
                System.out.print("Level: ");
                tree.levelorder();
                System.out.println();
            }
        } catch (IOException ex)
        {
            System.out.println("Error reading input");
        }
    }
}