/**
 * Created by IntelliJ Idea
 * User: jzbrooks
 * Date: 2/12/14
 * Time: 10:00 PM
 */
public class NBTree {
    Node root;
    Stack stack;
    int counter = 0;

    /**
     * Creates a new Non-Binary Tree
     */
    public NBTree(){
        root = new Node(null,null,null,null);
        stack = new Stack();
    }

    /**
     * Build template from two strings
     * @param pre preorder traversal spring
     * @param post postorder traversal spring
     * @return A list annotated by the index in the other traversal
     */
    public static List buildTemplate(String pre, String post) {
        List preorder_list = new List();
        for(int i=0; i<pre.length(); i++)
        {
            Character ch = pre.charAt(i);
            int idx = post.indexOf(ch) + 1;
            preorder_list.append(ch.toString(),idx);
        }
        return preorder_list;
    }

    /**
     * Recursively builds tree
     * @param pre annotated preorder list
     */
    public void buildTree(List pre) {
        root.key = pre.front.key;
        buildTree(root, pre);
    }

    private void buildTree(Node n, List list) {
        if(!list.has_one_item())
        {
            Node pred = list.pop();
            Node curr = list.first();
            if (pred.index > curr.index) {
                stack.push(pred.key, pred.index);
                Node child = addChild(n, curr.key);
                buildTree(child,list);
            } else {
                while(true) {
                    if(stack.top.index < curr.index) {
                        stack.deleteTop();
                        continue;
                    }
                    Node parent = search(stack.topKey());
                    Node child = addChild(parent, curr.key);
                    buildTree(child, list);
                    break;
                }
            }
        }
    }

    /**
     * Adds a child to tree
     * @param parent parent node
     * @param key string to be held by tree node
     * @return child node
     */
    public Node addChild(Node parent, String key) {
        if (parent.leftMostChild==null) {
            parent.leftMostChild = new Node(key,parent,null,null);
            parent.leftMostChild.isLeftMost = true;
            parent.leftMostChild.parent = parent;
            return parent.leftMostChild;
        }
        else {
            Node left_child = parent.leftMostChild;
            while (left_child.rightSibling!=null)
                left_child = left_child.rightSibling;
            left_child.rightSibling = new Node(key,parent,null,null);
            left_child.rightSibling.parent = parent;
            return left_child.rightSibling;
        }
    }

    /**
     * Recursively search tree
     * @param key string to be found
     * @return node if found, null otherwise
     */
    public Node search(String key) {
        return search(this.root,key);
    }

    private Node search(Node p, String key) {
           if (p.key == key) return p;
           for (Node child=p.leftMostChild; child!=null;
                child=child.rightSibling)
           {
               Node foundNode = search(child, key);
               if (foundNode != null) return foundNode;
           }
       return null;
    }

    /**
     * Preorder traversal
     */
    public void preorder() {
        System.out.print("(");
        preorder(this.root);
    }
    private void preorder(Node n) {
        if(n==null) return;
        if(n.isLeftMost) System.out.print("(");
        System.out.print(n.key);
        preorder(n.leftMostChild);
        preorder(n.rightSibling);
        if(!n.hasRightSibling()) System.out.print(")");
    }

    /**
     * Preorder traversal
     */
    public void postorder() {
        System.out.print("(");
        postorder(this.root);
    }

    private void postorder(Node n) {
        if(n==null) return;
        if(n.isLeftMost) System.out.print("(");
        postorder(n.leftMostChild);
        System.out.print(n.key);
        postorder(n.rightSibling);
        if(!n.hasRightSibling()) System.out.print(")");
    }

    /**
     * Level-Order traversal
     */
    public void levelorder() {
        Queue Z= new Queue();
        Queue Q2 = new Queue();
        System.out.print("("+root.key+")");
        Z.enqueue(root);
        Queue Q = getCousins(Z);
        while (!Q.isEmpty()) {
            System.out.print("(");
            while(!Q.isEmpty()) {
                Node m_dequeue = Q.dequeue();
                System.out.print(m_dequeue.key);
                Q2.enqueue(m_dequeue);
            }
            System.out.print(")");
            Q = getCousins(Q2);
        }
    }

    /**
     * Helper method to level order traversal
     * @param queue predecessor generation
     * @return Queue of subsequent generation
     */
    public Queue getCousins(Queue queue) {
        Queue q = new Queue();
        Queue queue2 = queue;
        while(!queue2.isEmpty()) {
            Node sibling = queue2.dequeue();
            if(sibling.leftMostChild!=null)
            {
                for(Node child=sibling.leftMostChild;
                    child!=null;child=child.rightSibling)
                    q.enqueue(child);
            }
        }
        return q;
    }
}